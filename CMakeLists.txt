PROJECT(conancryptlib)
cmake_minimum_required(VERSION 2.8)
include(conanbuildinfo.cmake)
CONAN_BASIC_SETUP()

option(BUILD_SHARED_LIBS "Build shared instead of static library" OFF)

set(HEADERS 
    include/brg_endian.h
    include/brg_types.h
    include/gcm.h
    include/gf128mul.h
    include/gf_mul_lo.h
    include/mode_hdr.h
)

set(SOURCES 
    src/gcm.c
    src/gf128mul.c
)

include_directories(include)

if(BUILD_SHARED_LIBS) 
    add_library(cryptlib SHARED ${SOURCES})
else()
    add_library(cryptlib STATIC ${SOURCES})
endif()